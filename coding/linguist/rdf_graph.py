from rdflib import URIRef, BNode, Literal

bob = URIRef("http://example.org/people/Bob")
linda = BNode() # a GUID is generated

name = Literal('Bob') # passing a string
age = Literal(24) # passing a python int
height = Literal(76.5) # passing a python float

from rdflib import Namespace

n = Namespace("http://example.org/people/")

n.bob # = rdflib.term.URIRef(u'http://example.org/people/bob')
n.eve # = rdflib.term.URIRef(u'http://example.org/people/eve')

from rdflib.namespace import RDF, FOAF

RDF.type
# = rdflib.term.URIRef(u'http://www.w3.org/1999/02/22-rdf-syntax-ns#type')

FOAF.knows
# = rdflib.term.URIRef(u'http://xmlns.com/foaf/0.1/knows')

from rdflib import Graph

g = Graph()

g.add( (bob, RDF.type, FOAF.Person) )
g.add( (bob, FOAF.name, name) )
g.add( (bob, FOAF.knows, linda) )
g.add( (linda, RDF.type, FOAF.Person) )
g.add( (linda, FOAF.name, Literal('Linda') ) )

print g.serialize(format='turtle')

g.load("some_foaf.rdf")
for s,p,o in g.triples( (None, RDF.type, FOAF.Person) ):
   print "%s is a person"%s

for s,p,o in g.triples( (None,  RDF.type, None) ):
   print "%s is a %s"%(s,o)

bobgraph = Graph()

bobgraph += g.triples( (bob, None, None) )

qres = g.query(
    """SELECT DISTINCT ?aname ?bname
       WHERE {
          ?a foaf:knows ?b .
          ?a foaf:name ?aname .
          ?b foaf:name ?bname .
       }""")

for row in qres:
    print("%s knows %s" % row)

from rdflib.namespace import FOAF

g.parse("http://danbri.livejournal.com/data/foaf")

for s,_,n in g.triples((None, FOAF['member_name'], None)):
    g.add((s, FOAF['name'], n))

