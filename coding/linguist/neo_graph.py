from bulbs.model import Node, Relationship
from bulbs.property import String, Integer, DateTime
from bulbs.utils import current_datetime

class Person(Node):

    element_type = "person"

    name = String(nullable=False)
    age = Integer()


class Knows(Relationship):

    label = "knows"

    created = DateTime(default=current_datetime, nullable=False)

################################################################################

>>> from people import Person, Knows
>>> from bulbs.neo4jserver import Graph
>>> g = Graph()

>>> g.add_proxy("people", Person)
>>> g.add_proxy("knows", Knows)

>>> james = g.people.create(name="James")
>>> julie = g.people.create(name="Julie")

>>> nodes = g.people.index.lookup(name="James")

>>> relationship = g.knows.create(james, julie)

>>> friends = james.outV('knows')

