#-*- encoding: utf-8 -*-

from neurochip.scripts.shortcuts import *

################################################################################

if __name__=='__main__':
    for prvd in [
        Intellect.provider('firefox', 'Load_JSON', path='/home/shivhack/Bureau/bookmarks-firefox.json'),

        Intellect.provider('pocket',  'Load_XML',  path='/home/shivhack/Bureau/ril_export.xml'),
        #Intellect.provider('pocket',  'API',       consumer='43192-0bf0c735f4890730e6f26996', token=''),

        Intellect.provider('mail',    'API',       host='imap.gmail.com', port=587, email='tayamino@gmail.com'),
    ]:
        prvd.synchronise()
