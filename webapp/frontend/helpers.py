import glob
import yaml
import json
import os
import sys
import time
import logging
from argparse import ArgumentParser

import sys
import time
import random
import datetime

import click

debug = os.environ.get('DEBUG', True)

def dbg(debug_string):
    if debug:
        logging.info(debug_string)

class UnknownChannel(Exception):
    pass

