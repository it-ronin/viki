from rest_framework.routers    import SimpleRouter

router = SimpleRouter()

from uchikoma.gestalt.core    import views as V_core
from uchikoma.gestalt.vortex  import views as V_vortex
from uchikoma.gestalt.thinker import views as V_thinker

router.register(r'^domains/$',                        V_core.DomainList,                        base_name='domain-list')
router.register(r'^domains/(?P<pk>[0-9]+)/$',         V_core.DomainDetail,                      base_name='domain-view')

router.register(r'^vortex/$',                         V_vortex.Homepage,                        base_name='vortex-home')

router.register(r'^linked/$',                         V_vortex.Linked.Homepage,                 base_name='linked-home')

router.register(r'^semantic/$',                       V_vortex.Semantic.Homepage,               base_name='semantic-home')
router.register(r'^rdf/namespaces/$',                 V_vortex.Semantic.RDF_NS.NamespaceList,   base_name='rdf-ns-list')
router.register(r'^rdf/namespaces/(?P<pk>[0-9]+)/$',  V_vortex.Semantic.RDF_NS.NamespaceDetail, base_name='rdf-ns-vie')

router.register(r'^snippets/$',                       V_thinker.SnippetList,                    base_name='snippet-list')
router.register(r'^snippets/(?P<pk>[0-9]+)/$',        V_thinker.SnippetDetail,                  base_name='snippet-view')

urlpatterns = router.urls

