import os, sys
import inspect

import requests
from urlparse import urlparse

from django.conf import settings, urls as djuri
from django.core.urlresolvers import reverse

from django.db import models
#from django.db import forms

from django.http import Http404
from django.template import RequestContext
from django.shortcuts import render_to_response

from django.utils      import timezone
from django.utils.text import slugify

import rest_framework.decorators
import rest_framework.response
import rest_framework.routers
import rest_framework.views
import rest_framework.viewsets

import rest_framework as REST

from rest_framework import status as REST_Status
from rest_framework.response import Response as REST_Response

