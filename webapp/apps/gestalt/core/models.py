from uchikoma.gestalt.core.utils import *

from django.contrib.auth.models import User, Group

################################################################################

class Domain(models.Model):
    alias      = models.CharField(max_length=128)

    #is_staff = models.BooleanProperty(default=False)
    #is_active = models.BooleanProperty(default=False)
    #is_superuser = models.BooleanProperty(default=False)

    #last_login = models.DateTimeProperty(default=timezone.now())
    #date_joined = models.DateTimeProperty(default=timezone.now())

#*******************************************************************************

class Verse(models.Model):
    domain     = models.ForeignKey(Domain, related_name='multiverse')
    alias      = models.CharField(max_length=128)

    namespaces = models.ManyToManyField('vortex.RdfNamespace',   related_name='namespaces')
    endpoints  = models.ManyToManyField('vortex.SparqlEndpoint', related_name='endpoints')

################################################################################

#class IntelliMind(models.Model):
#    verse       = models.ForeignKey(IntelliVerse, related_name='minds')
#    alias       = models.CharField(max_length=128)
#
#    topology    = models.CharField(max_length=128, choices=CORTEXs)
#    config      = models.TextField()
#
##*******************************************************************************
#
#class IntelliSpine(models.Model):
#    verse       = models.ForeignKey(IntelliVerse, related_name='minds')
#    alias       = models.CharField(max_length=128)
#
#    topology    = models.CharField(max_length=128, choices=SPINEs)
#    config      = models.TextField()
#
#    inputs      = models.ManyToManyField(IntelliMind, related_name='inputs')
#    outputs     = models.ManyToManyField(IntelliMind, related_name='outputs')
#
##*******************************************************************************
#
#class IntelliCase(models.Model):
#    mind        = models.ForeignKey(IntelliMind, related_name='cases')
#    released_on = models.DateTimeField(default=timezone.now())
#
#    title       = models.CharField(max_length=128, blank=True)
#    raw_data    = models.TextField()

