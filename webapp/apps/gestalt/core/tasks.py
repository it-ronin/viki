from django_rq import job

@job
def long_running_func():
    pass

#long_running_func.delay() # Enqueue function in "default" queue
#
#@job('high')
#def long_running_func():
#    pass
#
#long_running_func.delay() # Enqueue function in "high" queue

