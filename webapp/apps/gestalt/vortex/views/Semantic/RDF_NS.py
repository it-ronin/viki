from uchikoma.gestalt.vortex.shortcuts import *

################################################################################

#@Reactor.register_web('vortex', r'rdf/namespace/')
class NamespaceList(REST.views.APIView):
    """
    List all snippets, or create a new snippet.
    """
    def get(self, request, format=None):
        snippets = RdfNamespace.objects.all()
        serializer = RdfNamespaceSerializer(snippets, many=True)
        return REST_Response(serializer.data)

    def post(self, request, format=None):
        serializer = RdfNamespaceSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return REST_Response(serializer.data, status=REST_Status.HTTP_201_CREATED)
        return REST_Response(serializer.errors, status=REST_Status.HTTP_400_BAD_REQUEST)

#*******************************************************************************

@Reactor.register_web('vortex', r'rdf/namespace/(?P<pk>[0-9]+)/')
class NamespaceDetail(REST.views.APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    def get_object(self, pk):
        try:
            return RdfNamespace.objects.get(pk=pk)
        except Snippet.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = RdfNamespaceSerializer(snippet)
        return REST_Response(serializer.data)

    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = RdfNamespaceSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return REST_Response(serializer.data)
        return REST_Response(serializer.errors, status=REST_Status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return REST_Response(status=REST_Status.HTTP_204_NO_CONTENT)

################################################################################

urlpatterns = vHost.urlpatterns

