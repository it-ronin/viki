from uchikoma.gestalt.vortex.shortcuts import *

################################################################################

#@Reactor.register_web('vortex', r'')
class Homepage(REST.views.APIView):
    #authentication_classes = (authentication.TokenAuthentication,)
    #permission_classes = (permissions.IsAdminUser,)

    def get(self, request, format=None):
        #from .dashboard import HOME

        HOME = {
            'lg-8': (
                dict(provider='widgets/visits', title="Visits", heading="Based on a three months data", data=[
                    
                ], flags=dict(sizes=('sm-3','xs-6'), items=[
                    dict(title="Total Traffic", value=24541,    icon='users',       color='green'),
                    dict(title="Unique Users",  value=14778,    icon='bolt',        color='red'),
                    dict(title="Revenue",       value=3583.18,  icon='plus-square', color='green'),
                    dict(title="Total Sales",   value=59871.12, icon='user',        color='red'),
                ])),
                dict(provider='widgets/traffic', title="Traffic Sources", heading="One month tracking", fields=[
                    dict(label="Source", header="source-col-header"),
                    dict(label="Amount"),
                    dict(label="Change"),
                    dict(label="Percent.,%", header="hidden-xs"),
                    dict(label="Target"),
                    dict(label="Trend", header="chart-col-header hidden-xs"),
                ], rows=[
                    
                ]),
            ),
            'lg-4': (
                
            ),
        }

        return Response(HOME)

################################################################################

from . import RDF_NS
from . import SparQL

