from uchikoma.gestalt.utils import *

from SPARQLWrapper import SPARQLWrapper as SparqlWrapper
from SPARQLWrapper import JSON          as SparqlJSON

#*******************************************************************************

PROTOCOLs = (
    ('postgres', "PostgreSQL database"),
    ('mysql',    "MySQL database"),

    ('mongodb',  "MongoDB server"),
    ('couchdb',  "CouchDB server"),

    ('gremlin',  "Gremlin"),
    ('neo4j',    "Neo4j Cypher"),

    ('hdfs',     "Hadoop F.S"),

    ('elastic',  "Elastic Search"),
)

FORMATs = (
    ('json',     "JSON"),
    ('xml',      "XML"),
    ('yaml',     "YAML"),

    ('json-ld',  "JSON-LD"),
    ('rdf',      "RDF"),
    ('n3',       "N3 / Turtle"),

    ('geojson',  "GeoJSON"),
    ('xml',      "XML"),
    ('yaml',     "YAML"),
)

CORTEXs = (
    ('wernick', "Wernick"),
    ('broca',   "Broca"),

    ('reason',  "Reasoner"),
    ('learn',   "Learner"),
    ('assess',  "Assessement"),
    ('witness', "Witnessing"),
)

SPINEs = (
    ('acoustic', "Acoustic Nerve"),
    ('visual',   "Visual Nerve"),
)

################################################################################

class DataSet(object):
    def __init__(self, parent, narrow, data):
        self._prn = parent
        self._nrw = narrow
        self._dtn = data

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize()

    parent  = property(lambda self: self._prn)
    narrow  = property(lambda self: self._nrw)
    data    = property(lambda self: self._dtn)

    realm   = property(lambda self: self.parent.realm)
    db      = property(lambda self: self.parent.db)

    tribe   = property(lambda self: self.db[self.COLLECTION])

    def persist(self):
        pass

