#-*- coding: utf-8 -*-

from neurochip.shortcuts import *

################################################################################

HOME = [
    ('lg-8', [
        Widget('widgets/visits', title="Visits", heading="Based on a three months data", data=[
            
        ], flags=dict(sizes=('sm-3','xs-6'), items=[
            dict(title="Total Traffic", value=24541,    icon='users',       color='green'),
            dict(title="Unique Users",  value=14778,    icon='bolt',        color='red'),
            dict(title="Revenue",       value=3583.18,  icon='plus-square', color='green'),
            dict(title="Total Sales",   value=59871.12, icon='user',        color='red'),
        ])),
        Widget('widgets/traffic', title="Traffic Sources", heading="One month tracking", fields=[
            dict(label="Source", header="source-col-header"),
            dict(label="Amount"),
            dict(label="Change"),
            dict(label="Percent.,%", header="hidden-xs"),
            dict(label="Target"),
            dict(label="Trend", header="chart-col-header hidden-xs"),
        ], rows=[
            
        ]),
    ]), ('lg-4', [
        Widget('widgets/news', title="Newsfeed", heading="all combined", entries=[
            dict(
                sender=dict(full="Tikhon Laninga", mugshot='img/2.jpg'),
                receiver='',
                when='4 min',
                content="Hey Sam, how is it going? But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born",
            ),
        ]),
        Widget('widgets/chat', title="Instant Talk", heading="with John Doe", messages=[
            dict(
                sender=dict(full="Tikhon Laninga", mugshot='img/2.jpg'),
                receiver='',
                when='4 min',
                content="Hey Sam, how is it going? But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born",
            ),
        ]),
    ]),
]

