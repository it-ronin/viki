from django.contrib.auth.models import User, Group

from .models import *

################################################################################

class NamespaceSerializer(REST.serializers.HyperlinkedModelSerializer):
    class Meta:
        model = RdfNamespace
        #fields = ('id', 'title', 'code', 'linenos', 'language', 'style')

################################################################################

class EndpointSerializer(REST.serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SparqlEndpoint
        #fields = ('id', 'title', 'code', 'linenos', 'language', 'style')

