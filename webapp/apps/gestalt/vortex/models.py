from uchikoma.gestalt.vortex.utils import *

###################################################################################################

class DataSchema(models.Model):
    alias      = models.CharField(max_length=128)

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "Data schema"
        verbose_name_plural = "Data schemas"

#*******************************************************************************

class DataPredicate(models.Model):
    schema     = models.ForeignKey(DataSchema, related_name='predicates')
    alias      = models.CharField(max_length=128)

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "Data predicate"
        verbose_name_plural = "Data predicates"

#*******************************************************************************

class DataSource(models.Model):
    schema     = models.ForeignKey(DataSchema, related_name='mapping')
    alias      = models.CharField(max_length=128, default='main')

    adapter    = models.CharField(max_length=128, choices=PROTOCOLs, default='auto')
    link       = models.CharField(max_length=512)

    uri        = property(lambda self: urlparse(self.link))

    scheme     = property(lambda self: self.uri.scheme)
    hostname   = property(lambda self: self.uri.hostname)
    port       = property(lambda self: self.uri.port)
    username   = property(lambda self: self.uri.username)
    password   = property(lambda self: self.uri.password)
    path       = property(lambda self: self.uri.path)
    query      = property(lambda self: self.uri.query)

    location   = property(lambda self: self.uri.path[1:])
    queryset   = property(lambda self: urlparse_qs(self.query))

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "Data Source"
        verbose_name_plural = "Data Sources"

###################################################################################################
###################################################################################################

class RdfNamespace(models.Model):
    alias = models.CharField(max_length=64)
    link  = models.CharField(max_length=256)

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "RDF namespace"
        verbose_name_plural = "RDF namespaces"

#*******************************************************************************

class RdfOntology(models.Model):
    namespace = models.ForeignKey(RdfNamespace, related_name='ontologies')
    alias     = models.CharField(max_length=64)
    link      = models.CharField(max_length=256)

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "RDF ontology"
        verbose_name_plural = "RDF ontologies"

##*******************************************************************************
#
#class RdfPredicate(models.Model):
#    ontology = models.ForeignKey(RdfOntology, related_name='predicates')
#    alias    = models.CharField(max_length=64)
#    xsd_type = models.CharField(max_length=128)

##################################################################################################

class SparqlEndpoint(models.Model):
    alias      = models.CharField(max_length=64)
    link       = models.CharField(max_length=256)

    namespaces = models.ManyToManyField(RdfNamespace, related_name='endpoints')
    ns_listing = property(lambda self: ' , '.join([ns.alias for ns in self.namespaces.all()]))
    prefixes   = property(lambda self: '\n'.join([
        'PREFIX %(alias)s: <%(link)s>' % ns.__dict__
        for ns in self.namespaces.all()
    ]))

    #***************************************************************************

    def query(self, statement, *args, **kwargs):
        cmd = SPARQLWrapper(self.link)

        try:
            statement = statement % args
        except:
            try:
                statement = statement % kwargs
            except:
                pass

        statement = self.prefixes + '\n' + statement

        cmd.setQuery(statement)

        cmd.setReturnFormat(JSON)

        return cmd

    def execute(self, statement, *args, **kwargs):
        cmd = self.query(statement, *args, **kwargs)

        try:
            return cmd.query().convert()
        except:
            return None

    #***************************************************************************

    class Wrapper(DataSet):
        COLLECTION = 'sparql_ontology'

        def initialize(self):
            self.persist()

        def persist(self):
            cfg = dict(endpoint=self.link, narrow=self.narrow, ontology=self.data['class'], ancestor=self.data['parent'])

            doc = None

            try:
                doc = self.tribe.insert_one(cfg)
            except:
                pass

            #doc.save()

            return doc

    #***************************************************************************

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "SPARQL endpoint"
        verbose_name_plural = "SPARQL endpoints"

#*******************************************************************************

class SparqlOntology(models.Model):
    endpoint  = models.ForeignKey(SparqlEndpoint, related_name='ontologies')
    alias     = models.CharField(max_length=512)

    narrow    = models.TextField(blank=True)
    classe    = models.TextField(blank=True)
    parent    = models.TextField(blank=True)

    def __str__(self):     return str(self.narrow)
    def __unicode__(self): return unicode(self.narrow)

    class Meta:
        verbose_name        = "SparQL ontology"
        verbose_name_plural = "SparQL ontologies"

#*******************************************************************************

class SparqlQuery(models.Model):
    endpoint   = models.ForeignKey(SparqlEndpoint, related_name='queries')
    alias      = models.CharField(max_length=128)

    statement  = models.TextField()
    namespaces = models.ManyToManyField(RdfNamespace, related_name='queries')

    prefixes   = property(lambda self: [ns.alias for ns in self.namespaces.all()])

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "SPARQL query"
        verbose_name_plural = "SPARQL queries"

##################################################################################################

class CKAN_DataHub(models.Model): # CKAN backend
    alias   = models.CharField(max_length=64)
    realm   = models.CharField(max_length=256)
    version = models.PositiveIntegerField(default=3)

    #***************************************************************************

    def rpath(self, *parts): return '/'.join([self.realm, 'api', str(self.version)]+[x for x in parts])

    def request(self, verb, *path, **kwargs):
        handler = getattr(requests, verb.lower(), None)

        if callable(handler):
            req = handler(self.rpath(*path), **kwargs)

            return req.json()
        else:
            return {}

    #***************************************************************************

    class Wrapper(DataSet):
        COLLECTION = 'ckan_resource'

        def initialize(self):
            self.persist()

        def persist(self):
            cfg = dict(source=self.realm, narrow=self.narrow, raw=self.data)

            doc = None

            try:
                doc = self.tribe.insert_one(cfg)
            except:
                pass

            #doc.save()

            return doc

    #***************************************************************************

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "CKAN DataHub"
        verbose_name_plural = "CKAN DataHubs"

#*******************************************************************************

class CKAN_DataSet(models.Model): # CKAN dataset
    hub       = models.ForeignKey(CKAN_DataHub, related_name='datasets')
    alias     = models.CharField(max_length=256)

    when      = models.DateTimeField(default=timezone.now())
    raw       = models.TextField()
    details   = property(lambda self: json.loads(self.raw))

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "CKAN DataSet"
        verbose_name_plural = "CKAN DataSets"

#*******************************************************************************

class CKAN_Resource(models.Model): # CKAN resource
    dataset    = models.ForeignKey(CKAN_DataSet, related_name='resources')

    alias      = models.CharField(max_length=512)
    link       = models.CharField(max_length=512)
    summary    = models.TextField(blank=True, null=True, default=None)

    id_pkg     = models.CharField(max_length=512, blank=True, null=True, default=None)
    id_res     = models.CharField(max_length=512, blank=True, null=True, default=None)
    id_rev     = models.CharField(max_length=512, blank=True, null=True, default=None)

    type_pkg   = models.CharField(max_length=512, blank=True, null=True, default=None)
    type_res   = models.CharField(max_length=512, blank=True, null=True, default=None)
    type_url   = models.CharField(max_length=512, blank=True, null=True, default=None)

    when       = models.DateTimeField(default=timezone.now())
    raw        = models.TextField()
    details    = property(lambda self: json.loads(self.raw))

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "CKAN Resource"
        verbose_name_plural = "CKAN Resources"

