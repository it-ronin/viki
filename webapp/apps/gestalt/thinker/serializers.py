from django.contrib.auth.models import User, Group

from .models import *

################################################################################

class SnippetSerializer(REST.serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Snippet
        fields = ('id', 'title', 'code', 'linenos', 'language', 'style')

