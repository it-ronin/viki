from uchikoma.gestalt.shortcuts import *

################################################################################

from .models      import *
from .schemas     import *

from .forms       import *
from .serializers import *

################################################################################

@Reactor.register_vhost('thinker', ns='thinker')
class vHost(VirtualHost):
    pass

