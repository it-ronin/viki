#-*- coding: utf-8 -*-

import os, sys
from urlparse import urlparse

#*******************************************************************************

ROOT_PATH = __file__

for i in range(0, 4):
    ROOT_PATH = os.path.dirname(ROOT_PATH)

ROOT_PATH = os.path.abspath(ROOT_PATH)

FILES = lambda *x: os.path.join(ROOT_PATH, 'usr', *x)

################################################################################

SITE_ID = 1

USE_I18N = True
USE_L10N = True
USE_TZ = True

DEBUG = True
ADMINS = (
    ('TAYAA Med Amine', 'tayamino@gmail.com'),
)

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAdminUser',
    ),
    'PAGE_SIZE': 10,
}

#*******************************************************************************

def shell_link(key):
    if key in os.environ:
        resp = os.environ[key]

        if resp[0]=="'" and resp[-1]=="'":
            resp = resp[1:-2]

        return urlparse(resp)
    else:
        return None

#*******************************************************************************

DATABASES = {
    'local': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': FILES('main.sqlite'),
    },
}

if 'DATABASE_URL' in os.environ:
    link = shell_link('DATABASE_URL')

    DATABASES['heroku'] = {
        'ENGINE':   'django.db.backends.postgresql_psycopg2',
        "NAME":     link.path[1:],
        "HOST":     link.hostname,
        "PORT":     link.port,
        "USER":     link.username,
        "PASSWORD": link.password,
    }

#*******************************************************************************

NEO4J_DATABASES = {
    #'local' : {
    #    'HOST':'localhost',
    #    'PORT':7474,
    #    'ENDPOINT':'/db/data'
    #},
}

if 'GRAPHENEDB_URI' in os.environ:
    link = shell_link('GRAPHENEDB_URI')

    NEO4J_DATABASES['graphene'] = {
        "ENDPOINT": link.path,
        "HOST":     link.hostname,
        "PORT":     link.port,
        "USER":     link.username,
        "PASSWORD": link.password,
    }

#*******************************************************************************

MONGODB_DATABASES = {
    #"local": {
    #    "name":     'uchikoma_musashi',
    #    "host":     'localhost',
    #    "tz_aware": True, # if you using timezones in django (USE_TZ = True)
    #},
}

if 'MONGOLAB_URI' in os.environ:
    link = shell_link('MONGOLAB_URI')

    MONGODB_DATABASES['mongolab'] = {
        "name":     link.path[1:],
        "host":     link.hostname,
        "port":     link.port,
        "password": link.password,
        "username": link.username,
        "tz_aware": True, # if you using timezones in django (USE_TZ = True)
    }

################################################################################

def loop_config(target, *narrows):
    for nrw in narrows:
        if nrw in target:
            return target[nrw]

    return None

DATABASES['default']         = loop_config(DATABASES, 'heroku', 'local')
NEO4J_DATABASES['default']   = loop_config(NEO4J_DATABASES, 'graphene', 'local')
MONGODB_DATABASES['default'] = loop_config(MONGODB_DATABASES, 'mongolab', 'local')

DATABASE_ROUTERS = ['neo4django.utils.Neo4djangoIntegrationRouter']

################################################################################

BROKER_URL = os.environ.get('CLOUDAMQP_URL', 'amqp://guest:guest@localhost//')

#*******************************************************************************

RQ_QUEUES = {
    'default': {
        'URL': os.getenv('REDISTOGO_URL', 'redis://localhost:6379/0'), # If you're on Heroku
        'DEFAULT_TIMEOUT': 500,
#        'EXCEPTION_HANDLERS': ['path.to.my.handler'], # If you need custom exception handlers
    },
}

#CELERY_ACCEPT_CONTENT = ['json']
#CELERY_TASK_SERIALIZER = 'json'
#CELERY_RESULT_SERIALIZER = 'json'

################################################################################

TIME_ZONE = 'Africa/Casablanca'
LANGUAGE_CODE = 'fr-FR'

SECRET_KEY = 'i!6sv((h-v$!6qiqv=dtdht_m4lv!6o_o@l4bi5p#e3$3&amp;=z_s'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.admindocs',

    'django_extensions',
    #'django_mongoengine',
    'suit',
    'suit_rq',
    'django_rq',
    'rest_framework',

    'uchikoma.gestalt.core',
    'uchikoma.gestalt.vortex',
    'uchikoma.gestalt.thinker',
)

################################################################################

ALLOWED_HOSTS = []

SITE_ID = 1

USE_I18N = True
USE_L10N = True
USE_TZ = True

MEDIA_ROOT = FILES('media')
MEDIA_URL = '/media/'

STATIC_ROOT = FILES('static')
STATIC_URL = '/static/'

STATICFILES_DIRS = (
    FILES('assets'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'uchikoma.gestalt.urls'

WSGI_APPLICATION = 'uchikoma.gestalt.wsgi.application'

TEMPLATE_DIRS = (
    FILES('templates'),
)

################################################################################

MANAGERS = ADMINS
TEMPLATE_DEBUG = DEBUG

#*******************************************************************************

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

