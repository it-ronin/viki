from uchikoma.gestalt.helpers import *

################################################################################

class Component(object):
    def __init__(self, parent, alias=None, *args, **kwargs):
        self._prn = parent
        self._key = alias

        self.trigger('initialize', *args, **kwargs)

    parent = property(lambda self: self._prn)
    alias  = property(lambda self: self._key)

    def trigger(self, method, *args, **kwargs):
        handler = getattr(self, method, None)

        if callable(handler):
            return handler(*args, **kwargs)
        else:
            return None

################################################################################

from rest_framework.views import APIView

class Routable(Component):
    def initialize(self):
        self._rtr = REST.routers.DefaultRouter()

    router = property(lambda self: self._rtr)

    def register(self, handler, pattern, **opts):
        resp = handler

        #if inspect.isclass(resp):
        #    if issubclass(resp, APIView):
        if callable(getattr(resp, 'as_view', None)):
            #resp = resp.as_view()

            pass

        if 'name' in opts:
            opts['base_name'] = opts['name']

            del opts['name']

        if 'base_name' not in opts:
            opts['base_name'] = slugify(unicode(resp.__name__))

        if inspect.isfunction(resp):
            pass

        self.router.register(pattern, resp, **opts)

        return handler

#*******************************************************************************

class API_Manager(Routable):
    @property
    def urlpatterns(self):
        resp = [
            djuri.url(r'^', djuri.include(self.router.urls)),
        ]

        return resp

#*******************************************************************************

class Reactor(object):
    def __init__(self):
        self._vhs = {}

        #self._api = API_Manager(self, 'api')

    #API = property(lambda self: self._api)

    #***************************************************************************

    def register_vhost(self, *args, **kwargs):
        def do_apply(handler):
            resp = handler(self, *args, **kwargs)

            if resp.alias not in self._vhs:
                self._vhs[resp.alias] = resp

            return resp

        return lambda hnd: do_apply(hnd)

    def find_vhost(self, key, **opts):
        if key in self._vhs:
            return self._vhs.get(key, None)
        else:
            return None

    def reflect_vhost(self, method, vhost, *args, **kwargs):
        if vhost in self._vhs:
            entry = self._vhs[vhost]

            callback = getattr(entry, method, None)

            if callable(callback):
                return callback(*args, **kwargs)
        else:
            raise Exception()
            
        return None

    #***************************************************************************

    def register_api(self, *args, **kwargs):
        return self.reflect_vhost('register_api', *args, **kwargs)

    def register_web(self, *args, **kwargs):
        return self.reflect_vhost('register_web', *args, **kwargs)

#*******************************************************************************

Reactor = Reactor()

################################################################################

class VirtualDirectory(Routable):
    @property
    def urlpatterns(self):
        return self.router.urls

#*******************************************************************************

class VirtualHost(Component):
    def initialize(self, ns=None, **opts):
        self._ns = ns or self.alias

        self._vhs = {
            '/': VirtualDirectory(self, '/'),
        }

    NS   = property(lambda self: self._ns)
    Web  = property(lambda self: self._vhs)

    #***************************************************************************

    API  = property(lambda self: self.parent.API)
    Root = property(lambda self: self.Web['/'])

    @property
    def urlpatterns(self):
        resp = []

        for pattern,vdir in self.Web.iteritems():
            if pattern[0]=='/':
                pattern = pattern[1:]

            entry = djuri.url(r'^'+pattern, djuri.include(vdir.urlpatterns))

            resp.append(entry)

        print [vdir.urlpatterns for vdir in self.Web.values()]

        return resp

    #***************************************************************************

    def register_api(self, ptn, *args, **kwargs):
        return lambda hnd: self.API.register(hnd, self.NS+'/'+ptn, *args, **kwargs)

    def register_web(self, ptn, *args, **kwargs):
        return lambda hnd: self.Root.register(hnd, self.alias+'/'+ptn, *args, **kwargs)

