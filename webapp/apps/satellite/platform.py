#-*- coding: utf-8 -*-

from uchikoma.satellite.helpers import *
from uchikoma.satellite.specs import *

#*******************************************************************************

sys.path.insert(0, '../..')

#*******************************************************************************

ROOT_PATH = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

FILES = lambda *x: os.path.join(ROOT_PATH, 'files', *x)
NEUROCHIP = lambda *x: os.path.join(ROOT_PATH, 'neurochip', *x)

################################################################################

SITE_ID = 1

USE_I18N = True
USE_L10N = True
USE_TZ = True

MANAGERS = ADMINS
TEMPLATE_DEBUG = DEBUG

#*******************************************************************************

SESSION_COOKIE_DOMAIN = '.'+DEFAULT_HOST

LOGIN_URL = '//connect.%s/login' % DEFAULT_HOST
LOGIN_REDIRECT_URL = '//connect.%s/done' % DEFAULT_HOST

SOCIAL_AUTH_USERNAME_FORM_URL = '//connect.%s/signup/username' % DEFAULT_HOST
SOCIAL_AUTH_EMAIL_FORM_URL = '//connect.%s/signup/email' % DEFAULT_HOST
SOCIAL_AUTH_EMAIL_VALIDATION_URL = '//connect.%s/email/sent' % DEFAULT_HOST

#*******************************************************************************

MEDIA_URL = '//cdn.%s/media/' % DEFAULT_HOST
STATIC_URL = '//cdn.%s/static/' % DEFAULT_HOST

MEDIA_ROOT = FILES('media')
STATIC_ROOT = FILES('static')

STATICFILES_DIRS = (
    FILES('assets'),
    #FILES('cdn'),
)

TEMPLATE_DIRS = (
    FILES('templates'),
)

################################################################################

def shell_link(key):
    if key in os.environ:
        resp = os.environ[key]

        if resp[0]=="'" and resp[-1]=="'":
            resp = resp[1:-2]

        return urlparse(resp)
    else:
        return None

#*******************************************************************************

DATABASES = {
    #'local': {
    #    'ENGINE': 'django.db.backends.sqlite3',
    #    'NAME': FILES('main.sqlite'),
    #},
}

if 'DATABASE_URL' in os.environ:
    link = shell_link('DATABASE_URL')

    DATABASES['heroku'] = {
        'ENGINE':   'django.db.backends.postgresql_psycopg2',
        "NAME":     link.path[1:],
        "HOST":     link.hostname,
        "PORT":     link.port,
        "USER":     link.username,
        "PASSWORD": link.password,
    }

#*******************************************************************************

NEO4J_DATABASES = {
    #'local' : {
    #    'HOST':'localhost',
    #    'PORT':7474,
    #    'ENDPOINT':'/db/data'
    #},
}

if 'GRAPHENEDB_URI' in os.environ:
    link = shell_link('GRAPHENEDB_URI')

    NEO4J_DATABASES['graphene'] = {
        "ENDPOINT": link.path,
        "HOST":     link.hostname,
        "PORT":     link.port,
        "USER":     link.username,
        "PASSWORD": link.password,
    }

#*******************************************************************************

MONGODB_DATABASES = {
    #"local": {
    #    "name":     'uchikoma_musashi',
    #    "host":     'localhost',
    #    "tz_aware": True, # if you using timezones in django (USE_TZ = True)
    #},
}

if 'MONGOLAB_URI' in os.environ:
    link = shell_link('MONGOLAB_URI')

    MONGODB_DATABASES['mongolab'] = {
        "name":     link.path[1:],
        "host":     link.hostname,
        "port":     link.port,
        "password": link.password,
        "username": link.username,
        "tz_aware": True, # if you using timezones in django (USE_TZ = True)
    }

################################################################################

def loop_config(target, *narrows):
    for nrw in narrows:
        if nrw in target:
            return target[nrw]

    return None

DATABASES['default']         = loop_config(DATABASES, 'heroku', 'local')
NEO4J_DATABASES['default']   = loop_config(NEO4J_DATABASES, 'graphene', 'local')
MONGODB_DATABASES['default'] = loop_config(MONGODB_DATABASES, 'mongolab', 'local')

DATABASE_ROUTERS = ['neo4django.utils.Neo4djangoIntegrationRouter']

################################################################################

BROKER_URL = os.environ.get('CLOUDAMQP_URL', 'amqp://guest:guest@localhost//')
PUBSUB_URL = os.environ.get('CLOUDMQTT_URL', 'mqtt://localhost//')

#*******************************************************************************

CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

