from __future__ import absolute_import

from celery import shared_task

import os

#from django.utils import timezone

import simplejson as json

#*******************************************************************************

ROOT_PATH = os.path.dirname(__file__)

################################################################################

@shared_task
def scan_pocket(pseudo, token):
    import pocket

    api = pocket.Pocket(settings.SOCIAL_AUTH_POCKET_KEY, token)

    query = ('state','favorite','tag','contentType','sort','detailType','search','domain','since','count','offset')

    for entry in api.get():
        entry = {"status":1,"list":{"229279689":{"item_id":"229279689",
"resolved_id":"229279689",
"given_url":"http:\/\/www.grantland.com\/blog\/the-triangle\/post\/_\/id\/38347\/ryder-cup-preview",
"given_title":"The Massive Ryder Cup Preview - The Triangle Blog - Grantland",
"favorite":"0",
"status":"0",
"resolved_title":"The Massive Ryder Cup Preview",
"resolved_url":"http:\/\/www.grantland.com\/blog\/the-triangle\/post\/_\/id\/38347\/ryder-cup-preview",
"excerpt":"The list of things I love about the Ryder Cup is so long that it could fill a (tedious) novel, and golf fans can probably guess most of them.",
"is_article":"1",
"has_video":"1",
"has_image":"1",
"word_count":"3197",
"images":{"1":{"item_id":"229279689","image_id":"1",
	"src":"http:\/\/a.espncdn.com\/combiner\/i?img=\/photo\/2012\/0927\/grant_g_ryder_cr_640.jpg&w=640&h=360",
	"width":"0","height":"0","credit":"Jamie Squire\/Getty Images","caption":""}},
"videos":{"1":{"item_id":"229279689","video_id":"1",
	"src":"http:\/\/www.youtube.com\/v\/Er34PbFkVGk?version=3&hl=en_US&rel=0",
	"width":"420","height":"315","type":"1","vid":"Er34PbFkVGk"}}}}}

#*******************************************************************************

@shared_task
def scan_sparql(alias):
    from uchikoma.satellite.vortex.models import SparqlEndpoint

    ep = SparqlEndpoint.objects.get(alias=alias)

    data = ep.execute('''SELECT ?target ?classe ?parent
WHERE {
  ?target rdf:type ?classe.
  ?target rdfs:subClassOf ?parent
}
GROUP BY ?target''')

    if type(data) in (str,unicode):
        pass
    elif type(data) in (dict,):
        if data['results']:
            print "#) Getting list of RDF ontologies on '%s' :" % ep.link

            for row in data['results']['bindings']:
                print "\t-> %(value)s ..." % row['target']

                onto,st = ep.ontologies.get_or_create(alias=row['target']['value'])

                onto.narrow = row['target']['value']
                onto.classe = row['classe']['value']
                onto.parent = row['parent']['value']

                #onto.narrow = json.dumps(row['target'])
                #onto.classe = json.dumps(row['classe'])
                #onto.parent = json.dumps(row['parent'])

                onto.save()

                #self.process(self.Wrapper(self, row['target']['value'], row))

#*******************************************************************************

@shared_task
def scan_ckan(alias):
    from uchikoma.satellite.vortex.models import CKAN_DataHub

    hub = CKAN_DataHub.objects.get(alias=alias)

    data = hub.request('GET', 'action', 'package_list')

    if data['success']:
        print "#) Getting list of CKAN datasets on '%s' :" % hub.realm

        for key in data['result']:
            data = hub.request('GET', 'action', 'package_show?id='+key)

            if data['success']:
                print "\t-> %s ..." % key

                ds,st = hub.datasets.get_or_create(alias=key)

                ds.save()

                for entry in data['result']['resources']:
                    print "\t\t*) %(name)s ..." % entry

                    res,st = ds.resources.get_or_create(id_res=entry['id'])

                    for source,target in {
                        'name':          'alias',
                        'url':           'link',
                        'description':   'summary',

                        'package_id':    'id_pkg',
                        #'id':            'id_res',
                        'revision_id':   'id_rev',

                        'format':        'type_pkg',
                        'resource_type': 'type_res',
                        'url_type':      'type_url',
                    }.iteritems():
                        if source in entry:
                            setattr(res, target, entry[source])

                    res.save()

                    #self.process(hub.Wrapper(self, res['id'], res))

################################################################################

def cortex_config(self):
    print "\t-> Configuring node : %(uid)s" % nrw

    for key in config.keys():
        app.config[key] = config[key]

    return app

#*******************************************************************************

def cortex_deploy(self):
    print "\t-> Deploying node : %(uid)s" % nrw

    if not os.path.exists('src'):
        os.system('git clone git@bitbucket.org:tayaa/cortex.git temp --recursive')

        os.chdir('temp')

    os.chdir('%s/src' % ROOT_PATH)
    os.system('git remote -v')
    os.system('git remote rm heroku')
    os.system('git remote add heroku git@heroku.com:%s.git' % (GRID_PATTERN % nrw))
    os.system('git push heroku master')

    for svc in ('web','worker'):
        if svc in app.processes:
            app.processes[svc].scale(1)

    return app

