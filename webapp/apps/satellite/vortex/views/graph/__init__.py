#-*- coding: utf-8 -*-

from uchikoma.satellite.vortex.views.shortcuts import *

################################################################################

@render_to('tld/graph/pages/home.html')
def homepage(request):
    if not request.user.is_authenticated():
        return redirect('//connect.uchikoma.satellite.xyz/login') # redirect('done')

    return context(
        opendata={
            'ckan': {
                'datahub':   CKAN_DataHub.objects.all(),
                'dataset':   CKAN_DataSet.objects.all(),
                'resource':  CKAN_Resource.objects.all(),
            },
            'rdf': {
                'namespace': RdfNamespace.objects.all(),
                'ontology':  RdfOntology.objects.all(),
            },
            'sparql': {
                'endpoint':  SparqlEndpoint.objects.all(),
                'ontology':  SparqlOntology.objects.all(),
                'query':     SparqlQuery.objects.all(),
            },
        },
    )

################################################################################

def sparql_query(cnt):
    import rdflib

    cnt['sentence'] = nltk.word_tokenize(cnt['query']['content'])

    return cnt

#*******************************************************************************

@render_to('tld/graph/pages/query.html')
def rdf_query(request):
    if not request.user.is_authenticated():
        return redirect('//connect.uchikoma.satellite.xyz/login') # redirect('done')

    return lunch_program(request, forms.SparqlForm, sparql_query)

################################################################################

def rdf_elastic(cnt):
    import rdflib

    cnt['sentence'] = nltk.word_tokenize(cnt['query']['content'])

    return cnt

#*******************************************************************************

@render_to('tld/graph/pages/search.html')
def rdf_search(request):
    if not request.user.is_authenticated():
        return '//connect.uchikoma.satellite.xyz/login' # redirect('done')

    return lunch_program(request, forms.ElasticForm, rdf_elastic)

################################################################################

def rdf_command(cnt):
    import rdflib

    cnt['sentence'] = nltk.word_tokenize(cnt['query']['content'])

    return cnt

#*******************************************************************************

@render_to('tld/graph/pages/console.html')
def rdf_shell(request):
    if not request.user.is_authenticated():
        return redirect('//connect.uchikoma.satellite.xyz/login') # redirect('done')

    return lunch_program(request, forms.ShellForm, rdf_command)

################################################################################

@render_to('tld/connect/pages/agora.html')
def rdf_graph(request, narrow='default'):
    if not request.user.is_authenticated():
        return redirect('//connect.uchikoma.satellite.xyz/login') # redirect('done')

    return context(partition=narrow)

def rdf_payload(request, narrow):
    mapping = {
        'people': dict(
            query=dict(
                source=['Person'],
                target=['Brand','Company'],
            ),
            custom=dict(
                Person=(lambda node,prop: prop['full']),
                Brand=(lambda node,prop: prop['title']),
            ),
            label=['name','full'],
        ),
        'networks': dict(
            query=dict(
                source=['OS','Appliance'],
                target=['Application','EmbeddedSystem','Platform'],
            ),
            custom=dict(
                OS=(lambda node,prop: prop['name']),
                Appliance=(lambda node,prop: prop['name']),
            ),
            label=['name','full'],
        ),
        'skills': dict(
            query=dict(
                source=['Person', 'Language','Framework'],
                target=['Platform','Application'],
            ),
            custom=dict(
                Person=(lambda node,prop: prop['full']),
                OS=(lambda node,prop: prop['name']),
                Appliance=(lambda node,prop: prop['name']),
            ),
            label=['name','full'],
        ),
    }

    if narrow in mapping:
        lookup = "MATCH s-[r]-t WHERE ("
        lookup += "s.ontology in ['%s']" % "','".join(mapping[narrow]['query']['source'])
        lookup += ") AND ("
        lookup += "t.ontology in ['%s']" % "','".join(mapping[narrow]['query']['target'])
        lookup += ") RETURN s,r"

        nodes,edges = view_cypher(VORTEX_DEVOPS, lookup, mapping[narrow]['label'], mapping[narrow]['custom'])

        return JsonResponse({
            'nodes': nodes,
            'edges': edges,
        })
    else:
        raise Http404()

