#-*- coding: utf-8 -*-

from uchikoma.satellite.vortex.views.shortcuts import *

################################################################################

@render_to('tld/linked/pages/home.html')
def homepage(request):
    if not request.user.is_authenticated():
        return '//connect.uchikoma.satellite.xyz/login' # redirect('done')

    return context()

#*******************************************************************************

@render_to('tld/linked/pages/ckan/home.html')
def ckan_homepage(request):
    if not request.user.is_authenticated():
        return '//connect.uchikoma.satellite.xyz/login' # redirect('done')

    return context(
        opendata={
            'ckan': {
                'datahub':   CKAN_DataHub.objects.all(),
                'dataset':   CKAN_DataSet.objects.all(),
                'resource':  CKAN_Resource.objects.all(),
            },
            'rdf': {
                'namespace': RdfNamespace.objects.all(),
                'ontology':  RdfOntology.objects.all(),
            },
            'sparql': {
                'endpoint':  SparqlEndpoint.objects.all(),
                'ontology':  SparqlOntology.objects.all(),
                'query':     SparqlQuery.objects.all(),
            },
        },
    )

################################################################################

@render_to('tld/linked/pages/ckan/hub.html')
def ckan_overview(request, hub_id):
    if not request.user.is_authenticated():
        return '//connect.uchikoma.satellite.xyz/login' # redirect('done')

    return context()

################################################################################

@render_to('tld/linked/pages/ckan/dataset.html')
def ckan_dataset(request, hub_id, ds_id):
    if not request.user.is_authenticated():
        return '//connect.uchikoma.satellite.xyz/login' # redirect('done')

    return context()

################################################################################

@render_to('tld/linked/pages/ckan/resource.html')
def ckan_resource(request, hub_id, ds_id, res_id):
    if not request.user.is_authenticated():
        return '//connect.uchikoma.satellite.xyz/login' # redirect('done')

    return context()

