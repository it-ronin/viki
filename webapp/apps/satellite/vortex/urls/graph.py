from django.conf.urls import patterns, include, url

urlpatterns = patterns('uchikoma.satellite.vortex.views.graph',
    url(r'^$',                                     'homepage'),

    url(r'^search$',                               'rdf_search'),
    url(r'^sparql$',                               'rdf_query'),
    url(r'^shell$',                                'rdf_shell'),

    url(r'^explore/$',                             'rdf_graph'),
    url(r'^explore/(?P<narrow>[^/]+)/$',           'rdf_graph'),
    url(r'^explore/(?P<narrow>[^/]+)/graph.json$', 'rdf_payload'),
)

