from django.contrib import admin

from .models import *

##################################################################################################

class NamespaceAdmin(admin.ModelAdmin):
    list_display  = ('alias', 'link')
    list_editable = ('link',)

admin.site.register(RdfNamespace, NamespaceAdmin)

#*******************************************************************************

class OntologyAdmin(admin.ModelAdmin):
    list_display  = ('namespace', 'alias', 'link')
    list_filter   = ('namespace__alias',)
    list_editable = ('link',)

admin.site.register(RdfOntology, OntologyAdmin)

##################################################################################################

class EndpointAdmin(admin.ModelAdmin):
    list_display  = ('alias', 'link', 'prefixes')
    list_filter   = ('namespaces__alias',)
    list_editable = ('link',)

admin.site.register(SparqlEndpoint, EndpointAdmin)

#*******************************************************************************

class OntologyAdmin(admin.ModelAdmin):
    list_display  = ('endpoint', 'alias', 'narrow', 'classe', 'parent')
    list_filter   = ('endpoint__alias',)

admin.site.register(SparqlOntology, OntologyAdmin)

#*******************************************************************************

class QueryAdmin(admin.ModelAdmin):
    list_display  = ('endpoint','alias', 'statement', 'prefixes')
    list_filter   = ('endpoint__alias','namespaces__alias',)
    list_editable = ('statement',)

admin.site.register(SparqlQuery, QueryAdmin)

##################################################################################################

class CKAN_HubAdmin(admin.ModelAdmin):
    list_display  = ('alias', 'realm', 'version')
    list_filter   = ('version',)
    list_editable = ('realm', 'version')

admin.site.register(CKAN_DataHub, CKAN_HubAdmin)

#*******************************************************************************

class CKAN_SetAdmin(admin.ModelAdmin):
    list_display  = ('hub','alias', 'when')
    list_filter   = ('when','hub__version','hub__alias',)

admin.site.register(CKAN_DataSet, CKAN_SetAdmin)

#*******************************************************************************

class CKAN_ResAdmin(admin.ModelAdmin):
    list_display  = ('dataset','alias', 'type_pkg','type_res','type_url', 'when', 'id_pkg', 'id_res', 'id_rev')
    list_filter   = ('type_pkg','type_res','when','dataset__hub__version','dataset__hub__alias')
    #list_editable = ('realm', 'version')

admin.site.register(CKAN_Resource, CKAN_ResAdmin)

##################################################################################################

class SchemaAdmin(admin.ModelAdmin):
    list_display  = ('alias',)

admin.site.register(DataSchema, SchemaAdmin)

#*******************************************************************************

class PredicateAdmin(admin.ModelAdmin):
    list_display  = ('schema','alias')
    list_filter   = ('schema__alias',)

admin.site.register(DataPredicate, PredicateAdmin)

#*******************************************************************************

class SourceAdmin(admin.ModelAdmin):
    list_display  = ('schema','alias','adapter','link')
    list_filter   = ('schema__alias','adapter')
    list_editable = ('adapter','link')

admin.site.register(DataSource, SourceAdmin)

