#-*- coding: utf-8 -*-

from uchikoma.satellite.connect.views.shortcuts import *

################################################################################

@render_to('special/landing.html')
def custom_page(request):
    return context()

#*******************************************************************************

@render_to('special/feedback.html')
def contact_form(request):
    return context()

################################################################################

@render_to('tld/apis/pages/home.html')
def apis_doc(request):
    return context()

################################################################################

@render_to('tld/cdn/pages/home.html')
def back_stage(request):
    return context()

