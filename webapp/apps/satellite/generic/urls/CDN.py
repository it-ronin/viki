from django.conf.urls import patterns, include, url
from django.conf.urls.static import static

from django.contrib import admin
admin.autodiscover()

from neo4django import admin as neo_admin
neo_admin.autodiscover()

from django.conf import settings

urlpatterns = [
    url(r'^$',      'uchikoma.satellite.generic.landing.back_stage'),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^graph/', include(neo_admin.site.urls)),
] + static(
    '/static/', document_root=settings.STATIC_ROOT
) + static(
    '/media/', document_root=settings.MEDIA_ROOT
)

