from django.conf.urls import patterns, include, url

urlpatterns = patterns('uchikoma.satellite.generic.Landing',
    url(r'^$',        'custom_page'),
    url(r'^contact$', 'contact_form'),
)

