from uchikoma.satellite.shortcuts import *

from social.backends.oauth import BaseOAuth1, BaseOAuth2
from social.backends.google import GooglePlusAuth
from social.backends.utils import load_backends
from social.apps.django_app.utils import psa

from uchikoma.satellite.connect.decorators import render_to

#*******************************************************************************

def context(**extra):
    return dict({
        'plus_id': getattr(settings, 'SOCIAL_AUTH_GOOGLE_PLUS_KEY', None),
        'plus_scope': ' '.join(GooglePlusAuth.DEFAULT_SCOPE),
        'available_backends': load_backends(settings.AUTHENTICATION_BACKENDS)
    }, **extra)

#*******************************************************************************

import requests, simplejson as json

VORTEX_DEVOPS = 'http://scuba_dev:zdZ4ejSxxgMMtRdSwhcT@scubadev.sb05.stations.graphenedb.com:24789'

def view_cypher(target, lookup, labels=[], mapping={}):
    link = urlparse(target)

    req = requests.post('http://%s:%d/db/data/transaction/commit' % (link.hostname,link.port), data=json.dumps({
        "statements":[{
            "statement": lookup,
            "resultDataContents": ["row","graph"],
        }],
    }), auth=requests.auth.HTTPBasicAuth(link.username, link.password))

    resp = req.json()

    nodes, edges = {}, []

    vice = {}

    for result in resp['results']:
        for entry in result['data']:
            for n in entry['graph']['nodes']:
                extra = mapping.get(n['properties']['ontology'], None)

                extra = extra or (lambda n,p: p.values()[0])

                nodes[n['id']] = {
                    "label":  extra(n, n['properties']),
                    "fill":   "rgba(0,127,255,0.06)",
                    "stroke": "rgba(0,0,0,0.80)",
                }

            for e in entry['graph']['relationships']:
                edges.append([e['startNode'], e['endNode'], {"stroke":"rgba(0,0,0,0.60)"}])

    return nodes, edges

