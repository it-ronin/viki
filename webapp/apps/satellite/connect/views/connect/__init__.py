#-*- coding: utf-8 -*-

from uchikoma.satellite.connect.views.shortcuts import *

################################################################################

@render_to('special/login.html')
def do_login(request):
    if request.user.is_authenticated():
        return redirect('done')

    return context()

#*******************************************************************************

def do_logout(request):
    """Logs out user"""
    auth_logout(request)
    return redirect('//connect.uchikoma.satellite.xyz/')

################################################################################

@render_to('special/login.html')
def email_sent(request):
    return context(
        validation_sent=True,
        email=request.session.get('email_validation_address')
    )

#*******************************************************************************

@render_to('special/login.html')
def email_require(request):
    backend = request.session['partial_pipeline']['backend']
    return context(email_required=True, backend=backend)

################################################################################

@psa('social:complete')
def ajax_auth(request, backend):
    if isinstance(request.backend, BaseOAuth1):
        token = {
            'oauth_token': request.REQUEST.get('access_token'),
            'oauth_token_secret': request.REQUEST.get('access_token_secret'),
        }
    elif isinstance(request.backend, BaseOAuth2):
        token = request.REQUEST.get('access_token')
    else:
        raise HttpResponseBadRequest('Wrong backend type')
    user = request.backend.do_auth(token, ajax=True)
    login(request, user)
    data = {'id': user.id, 'username': user.username}
    return HttpResponse(json.dumps(data), mimetype='application/json')

################################################################################

@login_required
@render_to('tld/connect/pages/agora.html')
def agora_verse(request, narrow='people'):
    return context(partition=narrow)

def agora_graph(request, narrow):
    mapping = {
        'people': dict(
            query=dict(
                source=['Person'],
                target=['Brand','Company'],
            ),
            custom=dict(
                Person=(lambda node,prop: prop['full']),
                Brand=(lambda node,prop: prop['title']),
            ),
            label=['name','full'],
        ),
        'networks': dict(
            query=dict(
                source=['OS','Appliance'],
                target=['Application','EmbeddedSystem','Platform'],
            ),
            custom=dict(
                OS=(lambda node,prop: prop['name']),
                Appliance=(lambda node,prop: prop['name']),
            ),
            label=['name','full'],
        ),
        'skills': dict(
            query=dict(
                source=['Person', 'Language','Framework'],
                target=['Platform','Application'],
            ),
            custom=dict(
                Person=(lambda node,prop: prop['full']),
                OS=(lambda node,prop: prop['name']),
                Appliance=(lambda node,prop: prop['name']),
            ),
            label=['name','full'],
        ),
    }

    if narrow in mapping:
        lookup = "MATCH s-[r]-t WHERE ("
        lookup += "s.ontology in ['%s']" % "','".join(mapping[narrow]['query']['source'])
        lookup += ") AND ("
        lookup += "t.ontology in ['%s']" % "','".join(mapping[narrow]['query']['target'])
        lookup += ") RETURN s,r"

        nodes,edges = view_cypher(VORTEX_DEVOPS, lookup, mapping[narrow]['label'], mapping[narrow]['custom'])

        return JsonResponse({
            'nodes': nodes,
            'edges': edges,
        })
    else:
        raise Http404()

################################################################################

def homepage(request):
    if request.user.is_authenticated():
        return redirect('done')
    else:
        return redirect('login')

    return context()

#*******************************************************************************

@login_required
@render_to('tld/connect/pages/profile.html')
def profile(request):
    """Login complete view, displays user data"""
    return context()

#*******************************************************************************

@login_required
@render_to('tld/connect/pages/settings.html')
def settings(request):
    """Login complete view, displays user data"""
    return context()

