#-*- coding: utf-8 -*-

from uchikoma.satellite.connect.views.shortcuts import *

################################################################################

@render_to('tld/hub/pages/home.html')
def homepage(request):
    if not request.user.is_authenticated():
        return '//connect.uchikoma.satellite.xyz/login' # redirect('done')

    return context()

