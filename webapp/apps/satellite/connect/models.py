from django.contrib.auth.models import AbstractUser

from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate
from django.utils import timezone

from django.db import models

from urlparse import urlparse

#*******************************************************************************

class Identity(AbstractUser):
    pass
    #objects = UserManager()

    #username = models.StringProperty(indexed=True, unique=True)
    #first_name = models.StringProperty()
    #last_name = models.StringProperty()

    #email = models.EmailProperty(indexed=True)
    #password = models.StringProperty()

    #is_staff = models.BooleanProperty(default=False)
    #is_active = models.BooleanProperty(default=False)
    #is_superuser = models.BooleanProperty(default=False)

    #last_login = models.DateTimeProperty(default=timezone.now())
    #date_joined = models.DateTimeProperty(default=timezone.now())

    #USERNAME_FIELD = 'username'
    #REQUIRED_FIELDS=['email']

##################################################################################################

class Bot(models.Model):
    alias       = models.CharField(max_length=64)

    slack_token = models.CharField(max_length=512)

    @property
    def context(self):
        resp = dict(
            UCHIKOMA_SAT='uchikoma.xyz',
        )

        resp['UCHIKOMA_BOT']    = self.alias

        resp['API_SLACK_TOKEN'] = self.slack_token

        return resp

#*******************************************************************************

NEURO_ANATOMY = [
    ('face',       "Body :: Face"),

    ('broca',      "Linguist :: Broca"),
    ('wernick',    "Linguist :: Wernick"),

    ('dialect',    "Linguist :: Dialect"),
    ('glossary',   "Linguist :: Glossary"),

    ('rdf-graph',  "RDF :: Graph"),
    ('rdf-mapper', "RDF :: Mapper"),
    ('rdf-reason', "RDF :: Reasoner"),

    ('crawler',    "Misc :: Crawler"),
    ('spider',     "Misc :: Spider"),
    ('walker',     "Misc :: Walker"),
]

NEURO_MEMORY = [
]

#*******************************************************************************

class Cortex(models.Model):
    bot         = models.ForeignKey(Bot, related_name='lobes')
    alias       = models.CharField(max_length=64, blank=True)

    provider    = models.CharField(max_length=128, choices=NEURO_ANATOMY)
    config      = models.TextField(default='{}')

    @property
    def context(self):
        resp = self.bot.context

        resp['CORTEX_NARROW']   = self.alias

        resp['CORTEX_PROVIDER'] = self.provider
        resp['CORTEX_CONFIG']   = self.config

        return resp

#*******************************************************************************

class Memory(models.Model):
    bot         = models.ForeignKey(Bot, related_name='memories')
    alias       = models.CharField(max_length=64)

    @property
    def context(self):
        resp = self.bot.context

        resp['CORTEX_NARROW']   = self.alias

        resp['CORTEX_PROVIDER'] = self.provider
        resp['CORTEX_CONFIG']   = self.config

        return resp

NEURO_HUBs = (
    ('sparql', "SPARQL endpoint"),
    ('okfn',   "Open Knowledge Foundation"),
    ('ckan',   "Comprehensive Knowledge Archive Network"),
)

class Multiverse(models.Model):
    store       = models.ForeignKey(Bot, related_name='datahubs')
    alias       = models.CharField(max_length=64)

#*******************************************************************************

#class Cortex(models.NodeModel):
#    located_in = models.Relationship(Bot, single=True, rel_type='located_in', related_name='contains')
#    narrow     = models.StringProperty(indexed=True, unique=True)
#
#    topology   = models.StringProperty()
#    config     = models.StringProperty()

