from __future__ import absolute_import

from celery import shared_task

import os

#*******************************************************************************

ROOT_PATH = os.path.dirname(__file__)

################################################################################

@shared_task
def import_pocket(api_token):
    pass

#*******************************************************************************

@shared_task
def import_heroku(api_token):
    pass

################################################################################

def cortex_config(self):
    print "\t-> Configuring node : %(uid)s" % nrw

    for key in config.keys():
        app.config[key] = config[key]

    return app

#*******************************************************************************

def cortex_deploy(self):
    print "\t-> Deploying node : %(uid)s" % nrw

    if not os.path.exists('src'):
        os.system('git clone git@bitbucket.org:tayaa/cortex.git temp --recursive')

        os.chdir('temp')

    os.chdir('%s/src' % ROOT_PATH)
    os.system('git remote -v')
    os.system('git remote rm heroku')
    os.system('git remote add heroku git@heroku.com:%s.git' % (GRID_PATTERN % nrw))
    os.system('git push heroku master')

    for svc in ('web','worker'):
        if svc in app.processes:
            app.processes[svc].scale(1)

    return app

