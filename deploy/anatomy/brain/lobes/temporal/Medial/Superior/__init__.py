from uchikoma.abstract.anatomy.brain import *

################################################################################

class Cortex(Lobe):
    def before(self):
        self.publish(u'com.myapp.oncounter', self.counter)

    def process(self):
        counter += 1

    def after(self):
        yield sleep(1)

