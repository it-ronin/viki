from . import abstract

from . import cognito
from . import lobes
from . import body
from . import agents

#*******************************************************************************

from .abstract import *

################################################################################

class Conan(Tachikoma):
    domains=['common',
        ('scientia',1),
    ]

    def develop(self):
        yield body.Witness(parent=self, alias='slack')

        yield agents.Converse(parent=self, alias='slack')

        for x in range(0, 2):
            yield cognito.Learner(parent=self, alias='learn-%d' % x)

        for x in range(0, 2):
            yield cognito.Reasoner(parent=self, alias='reason-%d' % x)

#*******************************************************************************

class Loki(Tachikoma):
    domains=['common',
        ('business',1),
    ]

    def develop(self):
        yield body.Witness(parent=self, alias='slack')

        yield agents.Converse(parent=self, alias='slack')

        for x in range(0, 2):
            yield cognito.Learner(parent=self, alias='learn-%d' % x)

        for x in range(0, 2):
            yield cognito.Reasoner(parent=self, alias='reason-%d' % x)

#*******************************************************************************

class Max(Tachikoma):
    domains=['common',
        ('coding',1),
        ('devops',1),
    ]

    def develop(self):
        yield body.Witness(parent=self, alias='slack')

        yield agents.Converse(parent=self, alias='slack')

        for x in range(0, 2):
            yield cognito.Learner(parent=self, alias='learn-%d' % x)

        for x in range(0, 2):
            yield cognito.Reasoner(parent=self, alias='reason-%d' % x)

#*******************************************************************************

class Musashi(Tachikoma):
    domains=['common',
        ('devops',   1),
        ('internet', 1),
    ]

    def develop(self):
        yield body.Witness(parent=self, alias='slack')

        yield agents.Converse(parent=self, alias='slack')

        for x in range(0, 2):
            yield cognito.Learner(parent=self, alias='learn-%d' % x)

        for x in range(0, 2):
            yield cognito.Reasoner(parent=self, alias='reason-%d' % x)

#*******************************************************************************

class Proto(Tachikoma):
    domains=['common',
        ('metrics',  1),
    ]

    def develop(self):
        yield body.Behave(parent=self, alias='facebook')
        yield body.Witness(parent=self, alias='facebook')

        yield body.Witness(parent=self, alias='twitter')
        yield body.Witness(parent=self, alias='slack')

        yield agents.Converse(parent=self, alias='twitter')
        yield agents.Converse(parent=self, alias='slack')

        for x in range(0, 2):
            yield cognito.Learner(parent=self, alias='learn-%d' % x)

        for x in range(0, 2):
            yield cognito.Reasoner(parent=self, alias='reason-%d' % x)

