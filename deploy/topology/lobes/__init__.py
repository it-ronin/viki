from uchikoma.connect.flows.abstract import *

from uchikoma.connect.flows import cognito

################################################################################

class Broca(Lobe):
    def requires(self):
        return [
            cognito.RdfStore(parent=self, alias='facts'),
            cognito.Markov(parent=self, alias='flow'),
            cognito.Linguist(parent=self, alias='grammar'),
        ]

#*******************************************************************************

class Wernicke(Lobe):
    def requires(self):
        return [
            cognito.RdfStore(parent=self, alias='facts'),
            cognito.Markov(parent=self, alias='flow'),
            cognito.Linguist(parent=self, alias='grammar'),
        ]

